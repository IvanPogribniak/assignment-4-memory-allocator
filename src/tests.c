#include "tests.h"
#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"

#define test1_size_block1 1<<10
#define test1_size_block2 1<<9

#define test2_size_block1 1<<7
#define test2_size_block2 1<<12

#define test_3_size1 1<<10
#define test_3_size2 1<<14

#define initial_heap_size 1<<10

static void test_alloc_free(void* heap) {
    void* block1 = _malloc(test1_size_block1);
    void* block2 = _malloc(test1_size_block2);
    if (block1 == NULL || block2 == NULL) {
        debug("Unexpected NULL in _malloc.");
    }

    printf("Heap size 2^20, block1: 1024, block2: 512\n");
    debug_heap(stdout, heap);
    printf("Free first block\n");
    _free(block1);
    debug_heap(stdout, heap);
    printf("Free second block\n");
    _free(block2);
    debug_heap(stdout, heap);

}

static void test_grow_heap(void* heap) {
    printf("Running test 2\n");
    void* block1 = _malloc(test2_size_block1);
    printf("Trying to allocate a block sized 2^7 bytes\n");
    debug_heap(stdout, heap);
    void* block2 = _malloc(test2_size_block2);
    printf("Trying to allocate a block sized 2^12 bytes\n");
    printf("The heap should expand\n");
    debug_heap(stdout, heap);


    _free(block1);
    _free(block2);
}

static void test_add_region(void* heap) {
    printf("Allocating a region after the heap\n");
    alloc_region(heap + (1 << 13), test_3_size1);
    printf("Trying to allocate a block sized 2^14 bytes");
    _malloc(test_3_size2);
    debug_heap(stdout, heap);
}

void run_tests() {
    printf("Init a heap sized 2^10 bytes.\n");
    void *heap = heap_init(initial_heap_size);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        printf("Unexpected NULL in heap_init.\n");
    }
    test_alloc_free(heap);
    test_grow_heap(heap);
    test_add_region(heap);
}
