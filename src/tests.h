#ifndef MEMORY_ALLOCATOR_TESTS
#define MEMORY_ALLOCATOR_TESTS

#include <stddef.h>

void run_tests();

struct region alloc_region(void const *addr, size_t query);

#endif
